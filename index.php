<?php
require_once "vendor/autoload.php";

\Slim\Slim::registerAutoloader();

$app = new Slim\Slim();

$app->get('/hello', function (){
    echo "Hello";
});
$app->run();
